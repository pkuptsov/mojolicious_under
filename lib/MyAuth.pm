package MyAuth;
use Mojo::Base 'Mojolicious';

# This method will run once at server start
sub startup {
  my $self = shift;
  # session options set
  $self->secrets(['pr0t3ct3dk3yAr3a!!!#']);
  $self->sessions->default_expiration(86400);
  # Router
  my $r = $self->routes;

  # Normal route to controller
  $r->get('/')->to('example#welcome');
  $r->get('/auth')->to('example#auth');
  $r->get('/logout')->to('example#logout');
  # Auth router
  my $auth = $r->under(sub {
      my $apps = shift;
      if ($apps->session('user')) {
          return 1;
      }
      else {
          $apps->flash(alert => 'Not authenticated!');
          $apps->redirect_to('/') and return 0;
      }
  });
  # This is route can access only auth. users
  $auth->get('/protected')->to('example#protected');
  
}

1;
