package MyAuth::Controller::Example;
use Mojo::Base 'Mojolicious::Controller';

# This action will render a template
sub welcome {
  my $self = shift;

  # Render template "example/welcome.html.ep" with message
  $self->render(msg => 'Welcome to the Mojolicious real-time web framework!');
}

sub auth {
  my $self = shift;
  $self->session('user' => 'enable');
  $self->render(msg => 'This message only for auth. users');
}

sub logout {
  my $self = shift;
  $self->session(expires => 1);
  $self->redirect_to('/');
}

sub protected {
  my $self = shift;
  say "!!!!!!Wow!";
  $self->render(msg => 'This message only for auth. users');
}
1;
